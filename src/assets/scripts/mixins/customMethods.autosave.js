/**
 * Custom methods
 * -----------------------------------------------------------------------------
 *
 * @namespace customMethods
 */

// media queries in Javascript => http://wicky.nillia.ms/enquire.js/
import enquire from 'enquire.js';
// detect if a device is mouseOnly, touchOnly, or hybrid => http://detect-it.rafrex.com/
import detectIt from 'detect-it';
import '../vendor/meanmenu'
import '../vendor/main'
import '../vendor/jquery-scrolltofixed-min'
import Flickity from 'flickity';
import 'jquery-zoom'
  

const customMethods = {

  methods: {
    _initCustomMethods() {
        // custom JS code
        app._variantChangeImage()
        app._triggers()
        app._faqs()
        app._newsLetter()
        app._readMore()
        app._colorslider()
        app._checkout()
        app._colorSwatch()
        app._yotpo()
        app._arrowRotate()
        app._heightForSlider()
        app._sortingFilter()
        app._forDiscriptionBg()
        app._bundleApp()
        app._productNav()
        app._forCollectionsFilter()
        app._forFilter()
        app._zoom()
        app._customQTY()
        app._countColor()

      },
          _countColor(){
          setTimeout(function(){
             $('#app .index-featured-collectiion .product-slider .items').each(function(){
              var no_of_colors = $(this).find('.nav.color-picker li.nav-item .color-choose .form-check').length
              if(no_of_colors == 1 ){
                 $(this).find(".card-body .color-count").text(no_of_colors+ " Color")
              }
              else if(no_of_colors <= 0){
                $(this).find(".card-body .color-count").text()
              }
              else{
                $(this).find(".card-body .color-count").text(no_of_colors+ " Colors")
              }
           
          console.log($(this).find('.nav.color-picker li.nav-item .color-choose .form-check').length )
        })
           },500)  
       
      },
      _customQTY(){
        $('[data-decrease]').click(function(){
          var qty = $(this).siblings('[data-quantity]').val()
          if ( qty > 1 ) {
            qty = parseInt(qty)
            qty = qty - 1;
          }
          $(this).siblings('[data-quantity]').val(qty)
          $(this).closest('[data-main-role]').find('[data-linked-as]').val(qty)
          var form = $('form#cart_form').serialize()
          $.ajax({
            type: 'POST',
            async: false,
            url: '/cart/update.js',
            data: form,
            dataType: 'json',
            success: function(responce) { console.log(responce) }
          })
        })
        $('[data-increase]').click(function(){
          var qty = $(this).siblings('[data-quantity]').val()
          qty = parseInt(qty)
          qty = qty + 1;
          $(this).siblings('[data-quantity]').val(qty)
          $(this).closest('[data-main-role]').find('[data-linked-as]').val(qty)
          var form = $('form#cart_form').serialize()
          $.ajax({
            type: 'POST',
            async: false,
            url: '/cart/update.js',
            data: form,
            dataType: 'json',
            success: function(responce) { console.log(responce) }
          })
        })
      },

      _zoom(){
        // if($(window).width() > 1023){
        //   $('[data-image-zoom]')
        //   .wrap('<span style="display:inline-block"></span>')
        //   .css('display', 'block')
        //   .parent()
        //   .zoom({
        //     url: $(this).find('img').attr('data-zoom')
        //   });
        // }
      },

      _bundleApp(){
        $('[data-bundles] [data-for-size] li input').click(function(){
          var val = $(this).val()
          var select = $(this).closest('[data-one-product]').find('.bundle-select-wrapper select option')
          $(this).closest('.selector-wrapper').find('[data-sizecode]').text(': '+val)
          $(select).each(function(){
            var selVal = $(this).val()
            if(val == selVal){
              $(this).closest('.single-option-selector')
              .val(selVal)
              .trigger('change')
            }
          })
        })

        $('[data-bundles] [data-for-color] li input').click(function(){
          var val = $(this).val()
          var select = $(this).closest('[data-one-product]').find('.bundle-select-wrapper select option')
          $(this).closest('.selector-wrapper').find('[data-colorcode]').text(': '+val)
          $(select).each(function(){
            var selVal = $(this).val()
            if(val == selVal){
              $(this).closest('.single-option-selector')
              .val(selVal)
              .trigger('change')
            }
          })
        })
      },
  
      _variantChangeImage(){
        $("[data-for-simple] input").click(function(){
          var val = $(this).closest('.color-swatch').find('label').attr('data-original-title')
          if(val){
            let appendAll = '';
            var flkty1 = new Flickity('[data-carousel-nav]');
            flkty1.destroy()
            $('[data-thumbs]').each(function(index){
              var imgAlt = $(this).data('variant-title')
              imgAlt = imgAlt.replace('-',' ')
              if (imgAlt === val) {
                appendAll = appendAll + $(this).html()
              }
            })
            $('[data-carousel-nav]').removeClass('flickity-enabled is-draggable').empty()
            $('[data-carousel-nav]').append(appendAll)
            $('[data-carousel-nav] [data-variant-title]:first').addClass('is-nav-selected')
            $('[data-carousel-main] img').attr('src',$('[data-carousel-nav] [data-variant-title]:first').data('src').replace('_650x',''))
            $('[data-carousel-main] img').attr('data-zoom',$('[data-carousel-nav] [data-variant-title]:first').data('src').replace('_650x',''))

            const flky = new Flickity('[data-carousel-nav]', {
                "contain": true, 
                "draggable": true,
                "cellAlign": "left",
                "pageDots": false,
                "adaptiveHeight": true,
                "percentPosition": false,
                "freeScroll": false,
                "groupCells": true,
                "wrapAround": false,
                "arrowShape": { 
                    "x0": 20,
                    "x1": 60, "y1": 40,
                    "x2": 65, "y2": 35,
                    "x3": 30
                  }
              })
          }
        })
      },

      _yotpo(){
        var yotpoReviewText = $('.product-details-wrapper .yotpo .text-m').text().trim()
        if (yotpoReviewText != 'Write a review') {
          var yotpoReviewCount = $('.product-details-wrapper .yotpo .text-m').text().replace(' Reviews', '');
          $('.product-details-wrapper .yotpo .text-m').text('(' + yotpoReviewCount + ')');
          $('.product-details-wrapper .yotpo .text-m').show();
        }
      },

      _productNav(){
        $('body').on('click','[data-carousel-nav] [data-variant-title]',function(){
          $('[data-variant-title]').removeClass('is-nav-selected')
          $('[data-carousel-main] img').attr('src',$(this).data('src').replace('_650x',''))
          $('[data-carousel-main] img').attr('data-zoom',$(this).data('src').replace('_650x',''))
          $(this).closest('[data-variant-title]').addClass('is-nav-selected')
        })
      },

      _checkout(){
        $('[data-button-checkout]').click(function(){
          let discount = $('[data-discount]').val()
          window.location.href = '/checkout'+'?discount='+ discount
        });
        $('body').on('click','[data-scroll-color] [data-scroll-value]',function(){
          console.log($(this))
          var pos=$(this).position().left; //get left position of li
          var currentscroll=$(this).parents('li').scrollLeft(); // get current scroll position
          var divwidth=$(this).parents('li').width(); //get div width
          pos=(pos+currentscroll)-(divwidth/2); // for center position if you want adjust then change this
  
          $(this).parents('li').animate({
            scrollLeft: pos
          });
  
        });
      },

      _triggers(){
        $('[data-main-nav]').meanmenu();
        $('[data-menu-toggler]').click(function(){
            $('[data-mobile-navigation]').addClass('opened');
            $('[data-site-overlay]').addClass('show');
            $('body').addClass('body-cart-open');
        });

        $('[data-cart-drawer]').click(function(e){
          e.stopPropagation();
        });

        $('html, [data-menu-close]').click(function(){
            $('[data-mobile-navigation]').removeClass('opened');
            $('[data-site-overlay]').removeClass('show');
            $('body').removeClass('menu-open body-cart-open');
        });

        $('[data-mobile-navigation], [data-menu-toggler]').click(function(event){
            event.stopPropagation();
        });

        $('[data-size-close]').click(function(){
          $('#exampleModalCenter').modal('hide');
        });
        
        $('[data-clear-all]').click(function(){
          window.location.href = '/collections/all';
        });
        
      },

      _faqs(){
        $('[data-target-button]').click(function(){
            $(this).find('i').toggleClass('fa-plus').toggleClass('fa-minus');
        });
      },

      _newsLetter(){
        var $form = $('[data-newsletter]')
        if ( $form.length > 0 ) {
         $form.find('[data-subscribe]').bind('click', function ( event ) {
          if ( event ) event.preventDefault()
          function validate_input(){
  
           var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i
           var get_email = $form.find('[data-email-input]').val()
           if(get_email != ''){
               if (testEmail.test(get_email)){
                register($form)
               }else{
                 $form.find('[data-error-msg]').text('Enter valid email.')
                 $form.find('[data-error-msg]').show('slow')
               }          
            }
            else{
                $form.find('[data-error-msg]').text('Enter email.')
                $form.find('[data-error-msg]').show('slow')
            }
          }
          validate_input()
         })
        }
      
        function register($form) {
          $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          data: $form.serialize(),
          cache       : false,
          dataType    : 'json',
          contentType: "application/json; charset=utf-8",
          success     : function(data) {
            if (data.result != "success") {
            $form.find('[data-error-msg]').html(data.msg).show()
            } else {
            $form.find('[data-error-msg]').html(data.msg).show().delay(2000).fadeOut()
            $form.find('[data-email-input]').val('')
            }
          }
          })
        }
      },

      _readMore(){
        $("body").on("click","[data-readmore]",function(){
          $('[data-read-more]').removeClass('description-more');
          $(this).removeAttr('data-readmore').attr('data-readless','').text('less');
        });
        $("body").on("click","[data-readless]",function(){
          $("[data-read-more]").addClass('description-more');
          $(this).attr('data-readmore','').removeAttr('data-readless').text('more');
        });
      },

      _colorslider(){
        setTimeout(function(){
          $("[data-color-choose]").niceScroll();
        }, 2000);
        $('[data-sizecode]').text(': '+$('[data-for-size] input:checked').val())
        $('[data-colorCode]').text(': '+$('[data-for-color] input:checked').val())
      },

      _colorSwatch(){
        $('body').on('click','[data-color-choose] > div',function(){
            var pos=$(this).position().left; //get left position of li
            var currentscroll=$(this).parents('[data-color-choose]').scrollLeft(); // get current scroll position
            var divwidth=$(this).parents('[data-color-choose]').width(); //get div width
            pos=(pos+currentscroll)-(divwidth/2); // for center position if you want adjust then change this
        
            $(this).parents('[data-color-choose]').animate({
              scrollLeft: pos
            });
          });
      },
      
      _forFilter(){
        $('[data-filter-btn]').click((e) => {
          $('[data-collection-filters]').addClass('side_toggle');
          $('body').addClass('body_overlay');
        });
        $('[data-back-arrow]').click((e) => {
          
          $('[data-collection-filters]').removeClass('side_toggle');
          $('body').removeClass('body_overlay'); 
        });
      },

      _arrowRotate(){
        $('body').on('click','[data-single-option-selector],[data-filter-sort]',function(){
          $(this).toggleClass('for-arrow');
        });
      },

      _heightForSlider(){
        var artHeight = $('.product-slider .flickity-viewport').outerHeight();
        // console.log(artHeight);
        $('.product-slider .flickity-viewport').css({
          'height' : artHeight
        });
        $('.product-slider .flickity-viewport').animate({ // quick and dirty
          'opacity': 1
        });
      },

      _sortingFilter(){
        var url = location.href;
        url = url.split('?sort_by=')[1];
        $('[data-sort-heapbox] [data-active-sort]').text(url)
        $('[data-sort-heapbox] [data-sort-value] a').click(function(e){
          e.preventDefault()
          var val_sort = $(this).data('value') 
          $('[data-sort-heapbox] [data-active-sort]').text(val_sort)
          const sortValue = val_sort;
          if (sortValue) {
            Shopify.queryParams.sort_by = sortValue;
          } else {
            delete Shopify.queryParams.sort_by;
          }

          if (app.isCollectionAjaxed) {
            app._filterAjaxCall();
          } else {
            const search = $.param(Shopify.queryParams);
            if (search.length) {
              location.search = $.param(Shopify.queryParams);
            } else {
              location.href = location.pathname;
            }
          }
        })
      },

      _forDiscriptionBg(){
        setTimeout(() => {
          var height = $('.product-details-wrapper').outerHeight();
          $('[data-for-description-bg]').css('height',height);
        }, 1000);
      },

      _forCollectionsFilter(){
        // $('.collection__sidebar ').scrollToFixed({
        //   marginTop: function() {
        //       var marginTop = $(window).height() - $('.collection__sidebar').outerHeight(true) - 20;
        //       if (marginTop >= 0) return 20;
        //       return marginTop;
        //   }
        // });

        // var summaries = $('.collection__sidebar');
        //   summaries.each(function(i) {
        //     var summary = $(summaries[i]);
        //     var next = summaries[i + 1];

        //     summary.scrollToFixed({
        //         marginTop: $('header.site-header').outerHeight(true) - 70,
        //         limit: function() {
        //             var limit = 0;
        //             if (next) {
        //                 limit = $(next).offset().top - $(this).outerHeight(true) - 10;
        //             } else {
        //                 limit = $('footer').offset().top - $(this).outerHeight(true) - 40;
        //             }
        //             return limit;
        //         },
        //         zIndex: 999
        //     });
        // });
      },


      
    },
  }

export default customMethods;
