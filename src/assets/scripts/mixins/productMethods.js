/**
 * Product methods
 * -----------------------------------------------------------------------------
 *
 * @namespace productMethods
 */

import Flickity from 'flickity';
import 'flickity-as-nav-for';
import 'flickity-fullscreen';
import enquire from 'enquire.js';
import detectIt from 'detect-it';
import scrollMonitor from 'scrollmonitor';
import Vue from 'vue';

const productMethods = {

  methods: {
    /**
     * Product Single
     */
    _initProduct() {
      const $container = $('[data-template="product"]');
      const $productTabs = $(app.selectors.productTabs);
      const $productToolbarMobile = $(app.selectors.productToolbarMobile);
      const productObj = app.selectors.productObj;
      const promosArray = JSON.parse($('[data-promotions-json]').html());

      // product tabs add active class to 1st child
      $productTabs.find('.nav-item').first().find('a').addClass('active');
      $productTabs.find('.tab-content .tab-pane').first().addClass('active');

      // stop parsing if we are not in the product template
      if (app.currentTemplate !== 'product') {
        return;
      }

      // set data
      app.product = JSON.parse($(productObj).html());

      // assign the product cart limit
      app.product.tags.forEach((tag) => {
        if (tag.match('cart-limit')) {
          const cartLimit = tag.replace('cart-limit-', '');
          app.product.cart_limit = parseFloat(cartLimit);
          //console.log(app.product);
        }
        // assign the promotions data
        promosArray.forEach((promo) => {
          if (promo.tag === tag){
            app.product.message_product = promo.message_product
          }
        });
      });

      app.enableHistoryState = $container.data('enable-history-state');
      app._getVariant();

      // related products
      $(app.selectors.relatedProductObj).each((index, item) => {
        const productObj = $(item).html();
        const productData = JSON.parse(productObj);
        const product = productData.product;
        const optionsWithValues = productData.options_with_values;
        product.current_variant = product.variants[0];
        product.options_with_values = optionsWithValues;

        const selectedVarID = productData.product.id
        const jsonOfText = $('[data-' + selectedVarID + '-view-detail-json]')
        const jsonEleText = $(jsonOfText).text()
        if (jsonEleText) {
          const jsonEleData = JSON.parse(jsonEleText);
          if (jsonEleData.reviews_average != '') {
            Vue.set(product, 'reviews_average', jsonEleData.reviews_average)
          }
          if (jsonEleData.reviews_count != '') {
            Vue.set(product, 'reviews_count', jsonEleData.reviews_count)
          }
        }

        // assign the product cart limit
        product.tags.forEach((tag) => {
          if (tag.match('cart-limit')) {
            const cartLimit = tag.replace('cart-limit-', '');
            product.cart_limit = parseFloat(cartLimit);
            //console.log(product);
          }
        });

        app.relatedProducts.push(product);

      });

      function registerMobileToolbar() {
        const productFormPosition = $(app.selectors.productForm).position();
        const formWatcher = scrollMonitor.create($(app.selectors.productForm));
        const footerWatcher = scrollMonitor.create($('[data-footer-bottom]'));
        $productToolbarMobile.show();

        window.addEventListener('scroll', () => {
          const windowScrollY = window.scrollY;

          if (windowScrollY > productFormPosition.top) {
            formWatcher.exitViewport(() => {
              //console.log( 'I have left the viewport' )
              $productToolbarMobile.addClass('semi-opened');
            });

            formWatcher.enterViewport(() => {
              //console.log( 'I have entered the viewport' )
              $productToolbarMobile.removeClass('semi-opened fully-opened');
              if (app.isProductToolbarMobileOpen) {
                app.isProductToolbarMobileOpen = false;
              }
            });

            footerWatcher.exitViewport(() => {
              $productToolbarMobile.addClass('semi-opened');
            });

            footerWatcher.enterViewport(() => {
              $productToolbarMobile.removeClass('semi-opened fully-opened');
              if (app.isProductToolbarMobileOpen) {
                app.isProductToolbarMobileOpen = false;
              }
            });
          } else {
            $productToolbarMobile.removeClass('semi-opened fully-opened');
            if (app.isProductToolbarMobileOpen) {
              app.isProductToolbarMobileOpen = false;
            }
          }
        }); // add scroll event listener
      }

      // mobile product form - sticky toolbar
      enquire.register('screen and (min-width: 720px)', {
        setup() {
          registerMobileToolbar();
        },
        match() {
          $productToolbarMobile.hide();
        },
        unmatch() {
          registerMobileToolbar();
        },
      });
    },

    /**
     * Event when product option select or input changes
     *
     */
    _getVariant(event) {
      const variant = app._getVariantFromOptions(event);
      let formType;
      let colorType;

      if (event) {
        formType = $(event.currentTarget).closest(app.selectors.productForm).data('form-type');
        colorType = $(event.currentTarget).closest('.selector-wrapper').find('[data-for-color-product]');
      }
      $('[data-carousel-nav] [data-variant-title]:first').addClass('is-nav-selected')

      // if (variant && variant.available) {
      //   console.log('in')
      //   $('#BIS_trigger').hide(); // hide the button
      // } else {
      //   $('#BIS_trigger').show().attr('data-variant-id', variant.id); // show the button and set the default variant
      // }

      if (formType == 'product' && $(app.selectors.productForm).find('[data-for-color-product]') && variant) {
        if (variant.sku) {
          $('[data-sku]').text(variant.sku);
        }
        if (variant.barcode) {
          $('[data-barcode]').text(variant.barcode);
        }
      }

      if (variant == null) {
        Vue.set(app.currentVariant, 'unavailable', true)
      }else{
        Vue.set(app.currentVariant, 'unavailable', false)
      }

      // stop parsing if there is no variant info
      if (!variant) {
        return;
      }

      if ($(app.selectors.productForm).data('form-type') == 'product') {
        app._updateImages(variant, event);
      }
      app._checkWishlist(variant);

      if (app.isQuickshopModalOpen) {
        app.quickShop.currentVariant = variant;
      } else if (app.currentTemplate === 'collection' || app.currentTemplate === 'cart') { // collection
        const productId = $(event.currentTarget).closest(app.selectors.productForm).data('product-id');
        const product = app.collectionProducts.find(x => x.id === productId);
        const $variantImage = $(event.currentTarget).closest('.card').find('img');
        product.current_variant = variant;
      } else if (app.currentTemplate === 'product') {

        if (formType === 'listing') {
          const productId = $(event.currentTarget).closest(app.selectors.productForm).data('product-id');
          const product = app.relatedProducts.find(x => x.id === productId);
          const $variantImage = $(event.currentTarget).closest('.card').find('img');
          product.current_variant = variant;
        } else {
          app.currentVariant = variant;

          if (app.enableHistoryState) {
            app._updateHistoryState(variant);
          }

        }
      }
    },

    /**
     * Get the currently selected options from add-to-cart form.
     *
     * @return {array} options - Values of currently selected variants
     */
    _getCurrentOptions(event) {
      const singleOptionSelector = app.selectors.singleOptionSelector;
      const $container = event ? $(event.currentTarget).closest(app.selectors.productForm) : $(app.selectors.productForm);
      const currentOptions = [];
      let val;

      $.map($(singleOptionSelector, $container), (element) => {
        const $element = $(element);
        const type = $element.attr('type');
        const currentOption = {};

        if (type === 'radio' || type === 'checkbox') {
          if ($element[0].checked) {
            currentOption.value = $element.val();
            currentOption.index = $element.data('index');
            currentOptions.push(currentOption);
          }
        } else if (type == 'ul') {
          $(event.currentTarget).closest('form').find('[data-heapbox] li').removeClass('active')
          $(event.currentTarget).addClass('active')
          if ($(event.currentTarget).hasClass('active')) {
            val = $(event.currentTarget).find('span').text();
          }
          $(event.currentTarget).closest('[data-heapbox]').find('[data-active-size]').text(val);
          currentOption.value = $(event.currentTarget).closest('[data-product-form]').find('[data-heapbox] [data-active-size]').text();
          currentOption.index = $element.data('index');
          currentOptions.push(currentOption);
        } else {
          currentOption.value = $element.val();
          currentOption.index = $element.data('index');
          currentOptions.push(currentOption);
        }
      });

      // console.log(currentOptions);
      return currentOptions;
    },

    /**
     * Find variant based on selected values.
     *
     * @param  {array} selectedValues - Values of variant inputs
     * @return {object || undefined} found - Variant object from product.variants
     */
    _getVariantFromOptions(event) {
      const selectedValues = app._getCurrentOptions(event);
      let variants;
      let found = false;
      let formType;

      if (event) {
        formType = $(event.currentTarget).closest(app.selectors.productForm).data('form-type');
      }

      if (app.isQuickshopModalOpen) { // quickshop
        variants = app.quickShop.product.variants;
      } else if (app.currentTemplate === 'collection' || app.currentTemplate === 'cart') { // collection
        const productId = $(event.currentTarget).closest(app.selectors.productForm).data('product-id');
        const product = app.collectionProducts.find(x => x.id === productId);
        variants = product.variants;
      } else if (app.currentTemplate === 'product') { // product
        if (formType === 'listing') {
          const productId = $(event.currentTarget).closest(app.selectors.productForm).data('product-id');
          const product = app.relatedProducts.find(x => x.id === productId);
          variants = product.variants;
        } else {
          variants = app.product.variants;
        }

      }

      let selectedColor = $('[data-for-color] input:checked').val()
      $('.size-swatch').removeClass('not-available')
      let colorSoldout = []

     $('.size-swatch input').closest('li').addClass('d-none');
      
      variants.forEach((variant) => {
        let variant_title = variant.title
        if(variant.available == false){
          let soldout_title1 = variant_title.split('/')[0]
          let soldout_title2 = variant_title.split('/')[1]
          if(variant_title.indexOf(selectedColor) > -1){
            $('[data-for-size] input').each(function(){
              if($(this).val() == variant.option1 || $(this).val() == variant.option2){
                $(this).closest('.size-swatch').addClass('not-available')
              }
            })
          }
          if(soldout_title2 == null){
            colorSoldout.push(soldout_title1)
          }else{
            colorSoldout.push(soldout_title2)
          }
          colorSoldout.sort();
          var current = null;
          var cnt = 0;
          for (var i = 0; i < colorSoldout.length; i++) {
              if (colorSoldout[i] != current) {
                  current = colorSoldout[i];
                  cnt = 1;
              } else {
                  cnt++;
              }
              if(cnt == parseInt($("[data-for-size] > li").length)){
                $('input[value="'+$.trim(current)+'"]').closest('.color-swatch').addClass('not-color-available')
              }
          }
        }
        
        let satisfied = true;
        const option1 = variant.option1
        const option2 = variant.option2
        let selected = $('.color-swatch input:checked').val();
      
        if(option1 == $.trim(selected) || option2 == $.trim(selected)){
          $('.size-swatch input[value="'+variant.option1+'"]').closest('li').removeClass('d-none')
          $('.size-swatch input[value="'+variant.option2+'"]').closest('li').removeClass('d-none')
        }
        $("[data-for-color] .color-swatch input").on('change',function(){
         
          if($("[data-for-size] li.d-none:first").find('.size-swatch input[value="'+variant.option1+'"]').is(":checked") || $("[data-for-size] li.d-none:first").find('.size-swatch input[value="'+variant.option2+'"]').is(":checked") ){
            $("[data-for-size] li:not(.d-none):first").find('.size-swatch input').trigger('click').prop('checked',true);
          }
           
          // $("[data-for-size] li:not(.d-none):first").find('.size-swatch input[value="'+variant.option1+'"]').trigger('click').prop('checked',true);
          // $("[data-for-size] li:not(.d-none):first").not('.d-none').find('.size-swatch input[value="'+variant.option2+'"]').trigger('click').prop('checked',true);
        });

        selectedValues.forEach((option) => {
          if (satisfied) {
            satisfied = (option.value === variant[option.index]);
          }
        });
        if (satisfied) {
          found = variant;
        }
      });

      return found || null;
    },

    /**
     * Update history state for product deeplinking
     *
     * @param  {variant} variant - Currently selected variant
     * @return {k}         [description]
     */
    _updateHistoryState(variant) {
      if (!history.replaceState || !variant) {
        return;
      }

      let newUrl;

      const showReviewForm = app._getUrlParameter('showReviewForm');

      if (showReviewForm === '') {
        newUrl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}`;
      } else {
        newUrl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}&showReviewForm=${showReviewForm}`;
      }

      window.history.replaceState({
        path: newUrl,
      }, '', newUrl);
    },

    /**
     * Find variant image and scroll to this image
     * works for all product page templates
     * can be extended to update all images upon variant selection
     *
     * @param  {variant} variant - Currently selected variant
     */
    _updateImages(variant, event) {

      const variantImage = variant.featured_image || {};
      const variantTitle = variant.title.replace(/\/.*/, '').replace(/\s+$/, '');
      const templateType = $('[data-product-template]').data('product-template');
      const $scrollTarget = $(`[data-variant-title="${variantTitle}"]`);
      let $container;
      let currentVariantImage;

      if (app.isQuickshopModalOpen) {
        $container = $('[data-template="quickshop"]');
        currentVariantImage = app.quickShop.currentVariant.featured_image || {};
      } else if (app.currentTemplate === 'collection' || app.currentTemplate === 'cart') {
        $container = $(event.currentTarget).closest(app.selectors.productForm);
        const productId = $container.data('product-id');
        const product = app.collectionProducts.find(x => x.id === productId);
        if (product.current_variant.featured_image) {
          currentVariantImage = product.current_variant.featured_image.src || {};
        } else {
          currentVariantImage = product.featured_image || {};
        }
      } else if (app.currentTemplate === 'product') {
        $container = $('[data-template="product"]');
        currentVariantImage = app.currentVariant.featured_image || {};
      }

      // const $carousel = $container.find(app.selectors.carousel);
      // console.log($carousel)
      if (variantImage.src === currentVariantImage.src) {
        return;
      }

      enquire.register('screen and (min-width: 720px)', {
        match() {
          // For the 'minimal' product template
          // Scroll to variant image unless it's the first photo on load
          if (templateType === 'minimal' && !app.isQuickshopModalOpen) {
            $('html, body').animate({
              scrollTop: $scrollTarget.offset().top,
            }, 350);
          }
        },
      });


      // if ($carousel.length > 0) {
      //   const flkty = Flickity.data($carousel[0]);
      //   if (typeof flkty === 'undefined'){
      //     return;
      //   }
      //   const cellElements = flkty.getCellElements();

      //   // cellElements.forEach((element, index) => {
      //   //   const imgAlt = $(element).data('variant-title')
      //   //   if (imgAlt === variant.featured_image.alt) {
      //   //     // flkty.selectCell(index, true, false) // value, isWrapped, isInstant
      //   //   }
      //   // })
      // }

      // For image zoom 

      // if($(window).width() > 1023){
      //   $('[data-image-zoom]')
      //   .wrap('<span style="display:inline-block"></span>')
      //   .css('display', 'block')
      //   .parent()
      //   .zoom({
      //     url: $(this).find('img').attr('data-zoom')
      //   });
      // }
    },

    /**
     * Mobile toolbar - product form - options
     */
    _toggleProductToolbarMobile() {
      const $productToolbarMobile = $(app.selectors.productToolbarMobile);
      app.isProductToolbarMobileOpen = !app.isProductToolbarMobileOpen;
      $productToolbarMobile.toggleClass('fully-opened');
    },

  },
};

export default productMethods;