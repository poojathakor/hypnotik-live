/**
 * WPF Utilities Methods
 * -----------------------------------------------------------------------------
 *
 * @namespace utilitiesMethods
 */

import axios from 'axios';
import Flickity from 'flickity';

const utilitiesMethods = {
  data() {
    return {
      utilitiesReady: false,
      shop: {},
      shopAddons: {},
      addons: {
        loginHelper: {
          enabled: false,
          status: null,
          message: null,
          customerEmail: '',
          isLoginFormVisible: true,
          settings: {
            customerTag: null,
            customerTagMessage: null,
            customerActivateMessage: null,
            customerEnabledMessage: null,
            customerDisabledMessage: null,
          },
        },
        productAddons: {
          enabled: false,
          totalValue: null,
          currentAddons: {},
          settings: {
            displayStyle: 'grid',
          },
        },
        cartPromos: {
          enabled: false,
          enablerCodes: [],
          inCartTotalValue: 0,
          products: {},
          settings: {},
        },
      },
    };
  },

  methods: {
    async _initUtilities() {
      const shopData = await app._getShopData();

      if (!shopData){
        app.utilitiesReady = true;
        return;
      }

      await app._getShopAddons();
    },

    async _getShopData() {
      try {
        const shopData = await axios.get('/apps/store-utilities-proxy/getShopData');
        app.shop = shopData.data;
        return shopData;
      } catch (error) {
        console.log(Object.keys(error), error.message);
      }
    },

    async _getShopAddons() {
      let shopAddons;

      try {
        shopAddons = await axios.get('/apps/store-utilities-proxy/getShopAddons');
        app.shopAddons = shopAddons.data;
      } catch (error) {
        console.log(Object.keys(error), error.message);
      }

      app.utilitiesReady = true;

      if (!shopAddons || typeof shopAddons.data !== 'object') { // we don't have the addons stop parsing the utilities
        return;
      }

      app._initLoginHelper();
      app._initProductAddons();
      app._initCartPromos();
    },

    _initLoginHelper() {
      const loginHelper = app.shopAddons.find(x => x.type === 'loginHelper');
      let tempCustomerEmail = localStorage.getItem('customer_temp_email');

      if (tempCustomerEmail === 'null') {
        tempCustomerEmail = '';
      }

      app.addons.loginHelper.enabled = loginHelper.enabled;
      app.addons.loginHelper.settings = loginHelper.settings;
      app.addons.loginHelper.customerEmail = tempCustomerEmail;

      // Submit the form if the email field is not empty
      setTimeout(() => {
        if (app.addons.loginHelper.customerEmail !== '') {
          $('.shopify-warning').hide();
          app._submitLoginHelperForm();
        }
      }, 500);
    },

    _initProductAddons() {
      const productAddons = app.shopAddons.find(x => x.type === 'productAddons');

      if (app.currentTemplate === 'product') {
        const currentProductAddons = productAddons.data.products.find(x => x.id === app.product.id);

        if (typeof currentProductAddons === 'undefined') {
          return;
        }

        app.addons.productAddons.enabled = productAddons.enabled;
        app.addons.productAddons.settings = productAddons.settings;
        app.addons.productAddons.currentAddons = currentProductAddons.product_addons;
        app.addons.productAddons.currentAddons.enabled = currentProductAddons.enabled;

        if (productAddons.settings.displayStyle === 'carousel') {
          setTimeout(() => {
            const $carousel = $('[data-flickity-product-addons]');
            if ($carousel.length > 0) {
              const options = $carousel.data('flickity-product-addons');
              const flkty = new Flickity($carousel[0], options);
            }
          }, 100);
        }
      }
    },

    _initCartPromos() {
      app._setPromosEnablerCodes();

      if (app.currentTemplate !== 'cart' || !app.shopAddons) {
        return;
      }

      const cartPromos = app.shopAddons.find(x => x.type === 'cartPromos');
      //let promosCartTotalValue = 0;
      app.addons.cartPromos.enabled = cartPromos.enabled;
      app.addons.cartPromos.settings = cartPromos.settings;
      app.addons.cartPromos.products = cartPromos.data.products;

      // get all promos in cart
      const promosInCart = app.cart.items.filter(x => x.properties.promo_product === 'true');
      // set new totals
      const promosCartTotalValue = promosInCart.reduce((a, b) => +Number(a) + +Number(b.price), 0);
      app.addons.cartPromos.inCartTotalValue = promosCartTotalValue;
      app.cart.total_price -= promosCartTotalValue;
      app.cart.subtotal_price = app.cart.total_price;

      app.addons.cartPromos.products.forEach((product) => {
        app._checkPromoQualification(product);
      });
    },

    async _submitLoginHelperForm() {
      const formEl = $('#customer_login')[0];
      const formData = new FormData(formEl);
      const customerEmail = formData.get('customer[email]');
      const customerPassword = formData.get('customer[password]');
      localStorage.setItem('customer_temp_email', customerEmail);

      if (!customerEmail) {
        $('#customerEmail').addClass('is-invalid');
        return;
      }

      $('#customerEmail').removeClass('is-invalid');

      app.isLoading = true;
      const customerData = await axios.get(`/apps/store-utilities-proxy/getCustomerData?customer_email=${customerEmail}`);
      app.isLoading = false;
      app.addons.loginHelper.status = customerData.data.status;
      app.addons.loginHelper.message = customerData.data.message;
      //console.log(customerData.data.status);

      if (customerPassword) {
        formEl.submit();
      } else {
        $('#customerPassword').addClass('is-invalid');
      }
    },

    /**
     * Add product add-on
     */
    _addProductAddon(variantId, event) {
      const product = app.addons.productAddons.currentAddons.find(x => x.variants[0].id === variantId);
      app.addons.productAddons.totalValue += parseFloat(product.variants[0].price) * 100;
      product.selected = true;
    },

    /**
     * Remove product add-on
     */
    _removeProductAddon(variantId, event) {
      const product = app.addons.productAddons.currentAddons.find(x => x.variants[0].id === variantId);
      app.addons.productAddons.totalValue -= parseFloat(product.variants[0].price) * 100;
      product.selected = false;
    },

    /**
     * Promo products URL enabled codes
     */
    _setPromosEnablerCodes() {
      const storedEnablerCodes = JSON.parse(sessionStorage.getItem('cart_promos_enabler_codes')) || [];
      const newUserCode = app._getUrlParameter('promo_code');
      const storedUserCode = storedEnablerCodes.find(code => code === newUserCode);

      // push the user code from the url to the array in the session only if it doesn't exists
      if (newUserCode && !storedUserCode) {
        storedEnablerCodes.push(newUserCode);
        sessionStorage.setItem('cart_promos_enabler_codes', JSON.stringify(storedEnablerCodes));
      }
      // set the final codes
      app.addons.cartPromos.enablerCodes = storedEnablerCodes;
    },

    _getUpsellMessage(upsellMessage, qualifierAmount) {
      const cartTotal = parseFloat(app.cart.total_price / 100);
      const remainingAmount = Math.abs((cartTotal - parseFloat(qualifierAmount)) * 100);
      const remainingAmountMoney = app._formatMoney(remainingAmount);

      if (upsellMessage.indexOf('{amount}') >= 0) {
        upsellMessage = upsellMessage.split('{amount}');
        upsellMessage = `${upsellMessage[0]} ${remainingAmountMoney} ${upsellMessage[1]}`;
      }

      return upsellMessage;
    },

    /**
     * Check if cart promo product is qualified for the cart
     */
    _checkPromoQualification(product) {
      const cartItems = app.cart.items;
      const discountCode = app.addons.cartPromos.settings.discountCode;
      const qualifierType = product.qualifier_type;
      const qualifierAmount = product.minimum_cart_amount * 100;
      const isPromoInCart = cartItems.findIndex(x => x.product_id === product.id); // search if promo exists in cart items

      if (isPromoInCart >= 0) { // promo exists in cart => make the following checks
        product.in_cart = true;

        // if promo product is the only one in cart (orphan) remove it
        if (cartItems.length === 1) {
          app._removePromoFromCart(product.id);
        }

        // we need this to pass the discount code at the checkout URL
        // alters the cart form URL on the fly
        $(app.selectors.cartForm).attr('action', `/cart?discount=${discountCode}`);
      } else {
        product.in_cart = false;
      }

      // check qualification rules
      if (qualifierType === 'minimum cart amount') {
        product.visible = false;

        if (app.cart.total_price >= qualifierAmount) { // qualifier rules are valid
          product.qualified = true;
          app._checkPromoEnablerCode(product);
        } else { // qualifier rules are invalid
          product.qualified = false;

          if (product.enable_upsell) {
            app._checkPromoEnablerCode(product);
          } else {
            product.visible = false;
          }

          // qualifier rules no longer valid for a promo => remove it from cart
          if (isPromoInCart >= 0) {
            app._removePromoFromCart(product.id);
          }
        }
      } else {
        product.qualified = true;
        app._checkPromoEnablerCode(product);
      }
    },

    _checkPromoEnablerCode(product) {
      const productId = product.id;
      const variantId = product.variants[0].id;
      const addMode = product.add_mode;
      const cartItems = app.cart.items;
      const promoUrlEnablerCode = product.url_enabler_code;
      const matchedPromoCode = app.addons.cartPromos.enablerCodes.find(code => code === promoUrlEnablerCode);

      // enable with URL code (param) logic
      if (promoUrlEnablerCode) {
        product.visible = false;

        if (matchedPromoCode) {
          if (product.add_mode === 'auto' && product.qualified && cartItems.length > 0) {
            app._addPromoToCart(productId, variantId, addMode);
          } else {
            product.visible = true;
          }
        }
      } else if (product.add_mode === 'auto' && product.qualified && cartItems.length > 0) {
        app._addPromoToCart(productId, variantId, addMode);
      } else {
        product.visible = true;
      }
    },

    _addPromoToCart(productId, variantId, addMode) {
      const product = app.cart.items.find(item => item.product_id === productId);

      // add promo product to cart only if it's not already there
      if (!product) {
        app.isLoading = true;

        axios.post('/cart/add.js', {
            quantity: 1,
            id: variantId,
            properties: {
              promo_product: 'true',
              add_mode: addMode,
            },
          })
          .then((response) => {
            app.isLoading = false;

            if (!app.isCartAjaxed) {
              location.reload();
            } else {
              app._getCartData();
              app.$snotify.success('Gift added to cart. A discount estimate has been applied.', 'Free Gift', {
                timeout: 10000,
                showProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                buttons: [{
                  text: 'Close',
                  bold: true,
                  closeOnClick: true,
                }],
              });
            }
            return response;
          })
          .catch((error) => {
            // console.log(error)
            throw error;
          });
      }
    },

    _removePromoFromCart(productId) {
      const cartItemIndex = app.cart.items.findIndex(item => item.product_id === productId) + 1;
      app.isLoading = true;

      axios.post('/cart/change.js', {
          quantity: 0,
          line: cartItemIndex,
        })
        .then((response) => {
          app.isLoading = false;

          if (!app.isCartAjaxed) {
            location.reload();
          } else {
            app._getCartData();
            app.$snotify.info('Gift removed from cart.', 'Free Gift', {
              timeout: 10000,
              showProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              buttons: [{
                text: 'Close',
                bold: true,
                closeOnClick: true,
              }],
            });
          }
          return response;
        })
        .catch((error) => {
          // console.log(error)
          throw error;
        });
    },

  },
};

export default utilitiesMethods;
