/**
 * Custom methods
 * -----------------------------------------------------------------------------
 *
 * @namespace customMethods
 */

// media queries in Javascript => http://wicky.nillia.ms/enquire.js/
import enquire from 'enquire.js';
// detect if a device is mouseOnly, touchOnly, or hybrid => http://detect-it.rafrex.com/
import detectIt from 'detect-it';
import '../vendor/meanmenu'
import '../vendor/flexslider'
import '../vendor/main'
import '../vendor/jquery-scrolltofixed-min'
import Flickity from 'flickity';
import zoom from 'jquery-zoom'


const customMethods = {

  methods: {
    _initCustomMethods() {
        // custom JS code
        app._heroFlexSlider()
        app._variantChangeImage()
        app._triggers()
        app._faqs()
        app._newsLetter()
        app._readMore()
        app._colorslider()
        app._checkout()
        app._colorSwatch()
        app._yotpo()
        app._arrowRotate()
        app._heightForSlider()
        app._sortingFilter()
        app._forDiscriptionBg()
        app._bundleApp()
        app._productNav()
        app._customQTY()
        app._countColor()
        app._forFilter()
        app._playYoutubeVideo()
        app._addFreeProductToCart()
        // app._forCollectionsFilter()
        app._zoom()
      },
      _addFreeProductToCart(){
        $('body.template-cart').css('overflow','auto');
        console.log("In _addFreeProductToCart Function");
        $('[data-temp-checkout]').on('click', function(event){
          event.preventDefault();
          
            setTimeout(() => {
              $('[data-site-overlay]').css({"visibility": "visible", "opacity": "1"});
              $('body.template-cart').css('overflow','hidden');
            },500);
            $('.select-free-product-popup').fadeIn("500");
            // setTimeout(() => {
              if ( matchMedia('screen and (max-width: 768px)').matches ) {
                
              const $carousel1 = $('.free-product-slider')
              const options1 = {
                adaptiveHeight: false,
                draggable : true,
                wrapAround: false,
                groupCells: "100%",
                dragThreshold: 20,
                lazyLoad: true,
                prevNextButtons: true,
                pageDots: false,
                arrowShape: {x0: 10, x1: 60,y1: 50, x2: 60, y2: 45, x3: 15}
              };
              const flkty1 = new Flickity($carousel1[0], options1)
            } else{
              const $carousel1 = $('.free-product-slider')
              const options1 = {
                adaptiveHeight: false,
                draggable : false,
                wrapAround: false,
                groupCells: "100%",
                dragThreshold: 20,
                lazyLoad: true,
                prevNextButtons: true,
                pageDots: false,
                arrowShape: {x0: 10, x1: 60,y1: 50, x2: 60, y2: 45, x3: 15}
              };
              const flkty1 = new Flickity($carousel1[0], options1)
            }
            $('[free-product-add-to-cart]').on('click',function(event){
              $(this).attr('disabled','disabled');
              $('[free-product-add-to-cart]').text('Adding');
              // setTimeout(() => {
              //   $('[free-product-add-to-cart]').text('Add To Cart');
              // },2000);
              var checked_id = $(".select-free-product-popup .free-product-container .items input:checked").attr("data-id");
              $.ajax({
                type: 'POST',
                url: '/cart/add.js',
                data: 'quantity='+1+'&id='+checked_id,
                dataType: 'json',
                async:false,
                success: function() {
                  location.reload();
                  console.log("Hiii");
                },
                error: function(er) {
                    var err = $.parseJSON(er.responseText);
                    return false;
                }
              });

            });
        });
        $('[data-free-product-close-btn]').on('click', function(event){
          event.preventDefault();
            $('[data-site-overlay]').css({"visibility": "hidden", "opacity": "0"});
            $('.select-free-product-popup').hide();
            $('body.template-cart').css('overflow','auto');
        });
        if($('.select-free-product-popup').length > 0){
          $('.free-product-container .items:first input').prop( "checked" ,true);
        }

       
        // console.log(free_product);
        var all_tag;
        var tag_to_fetch_handle;
        var free_product_handle="";
        var free_product_index = ""
        jQuery.getJSON('/cart.js', function(cart) {
          $.each(cart.items, function( index, value ) {
                // console.log( index + ": " + value.handle );
                var Url = '/products/'+value.handle+'.json';
                $.ajax({
                  dataType: 'json',
                  url: Url,
                  async: false,
                  success: function(data)
                  {
                    var tag_list = data.product.tags;
                    tag_to_fetch_handle = tag_list.indexOf("is_hidden");
                    if(tag_to_fetch_handle > 0){
                      free_product_handle = value.handle;
                    }
                    all_tag += (tag_list);
                  }
              });
              if(value.handle == free_product_handle){
                free_product_index = index + 1;
              }
            }); 
            var index_of_tag = all_tag.indexOf("is_hidden");
            console.log(index_of_tag);
            if(index_of_tag > 0){
              if(free_product_handle != ""){
                $('[data-handle="'+free_product_handle+'"] .cart-price').html('<div class="text-red">Free</div>');
                $('[data-handle="'+free_product_handle+'"] .cart-qty-box').html('<div class="text-red free-prod-qty">1</div>');
                $('[data-handle="'+free_product_handle+'"] .cart-product a').css('pointer-events','none');
              }
            }
        }); 
      },
      _playYoutubeVideo(){
          $(document).on('click', '.play_button', function(e) {
              e.preventDefault();
              var poster = $(this);
              var wrapper = $('.video-container iframe.desktop_video');
              wrapper.addClass('videoWrapperActive');
              $(this).addClass('disableOverlay');
              var src = wrapper.data('src');
              wrapper.attr('src',src);
          });
      },
       _heroFlexSlider() {
        $('.header-slider').each(function(){
          var nav_dots = $(this).find('.nav-dots').attr('data-nav-dots');
          var nav_arrows = $(this).find('.nav-arrows').attr('data-nav-arrows');
          var autoplay = $(this).find('.autoplay').attr('data-autoplay');
          var slider_speed = 0;
          var enable_nav_dots = false;
          var enable_nav_arrows = false;
          if(nav_dots == "true"){
            enable_nav_dots = true;
          }
          if(nav_arrows == "true"){
            enable_nav_arrows = true;
          }
          var slider_show_enable = false;
          if(autoplay > 0){
            slider_show_enable = true;
            slider_speed = autoplay*1000;
          }
          $(this).find('.flexslider').flexslider({
            animation: "slide",
            slideshow: slider_show_enable,
            slideshowSpeed: slider_speed,
            controlNav: enable_nav_dots,
            directionNav: enable_nav_arrows
          })
        })
        
      },
          _countColor(){
            setTimeout(function(){
              $('[data-items]').each(function(){
                var no_of_colors = $(this).find('[data-color-choose] [data-scroll-value]').length
                if(no_of_colors == 1 ){
                    $(this).find("[data-color-count]").text(no_of_colors+ " Color")
                }
                else if(no_of_colors <= 0){
                    $(this).find("[data-color-count]").text()
                }
                else{
                    $(this).find("[data-color-count]").text(no_of_colors+ " Colors")
                }
              })
          },500)
      },

      _customQTY(){
        // $('[data-click-qty]').click(function() {
        //   var $form = $(this).closest('form');
        //   $form.find('[data-link-to]').each(function() {
        //       var $input = $(this);
        //       $($input.attr('data-link-to')).val($input.val());
        //   });
        //   $form.submit();
        // });

        $('[data-decrease]').click(function(){
          var qty = $(this).siblings('[data-quantity]').val()
          if ( qty > 1 ) {
            qty = parseInt(qty)
            qty = qty - 1;
          }
          $(this).siblings('[data-quantity]').val(qty)
          var $form = $(this).closest('form')
          $form.find('[data-link-to]').each(function() {
              var $input = $(this);
              $($input.attr('data-link-to')).val($input.val());
          });
          $form.submit()
        })

        $('[data-increase]').click(function(){
          var qty = $(this).siblings('[data-quantity]').val()
          qty = parseInt(qty)
          qty = qty + 1;
          $(this).siblings('[data-quantity]').val(qty)
          var $form = $(this).closest('form')
          $form.find('[data-link-to]').each(function() {
              var $input = $(this);
              $($input.attr('data-link-to')).val($input.val());
          });
          $form.submit()
        })
      },

      _zoom(){
        if($(window).width() > 1023){
          $('[data-image-zoom]')
          .wrap('<span style="display:inline-block" class="carousel-main"></span>')
          .css('display', 'block')
          .parent()
          .zoom({
            url: $(this).find('img').attr('data-zoom')
          });
        }
      },

      _bundleApp(){
        if($('#product-bundle').length > 0){
          $('[data-one-product]').each(function(i,e){
            var $this = $(this);
            var current_color = $this.find(".color-swatch input[type='radio']:checked").val()
            // console.log(current_color);
            var real_size_option_arr=[];
            var  option1_size_all_arr=[];
           
            $this.find("[data-for-size] li.list-inline-item").each(function(){
              $(this).removeClass('d-none');
              // $(this).find('.size-swatch input').val()
              var real_size_option = $(this).find('.size-swatch input').val();
              real_size_option_arr.push(real_size_option);
            });
            
            $this.find("[data-for-size] li.list-inline-item:not(.d-none):first input").trigger('click');
            $(this).find('[data-bundle-variant] option').each(function(i,e){
                var op_val_all = $.trim($(this).html());
                var op_val_arr_all = op_val_all.split("/");
                // console.log(op_val_arr_all, "============");
                var index_color_in_arr_all = $.trim(op_val_arr_all).indexOf(current_color);
                if(index_color_in_arr_all > -1){
                  var option1_all = $.trim(op_val_arr_all[0]);
                  var option2_all = $.trim(op_val_arr_all[1]);
                  // console.log(option1_all);
                  option1_size_all_arr.push(option1_all);
                }

                if ($(this).hasClass("unavailable")){
                  var op_val = $.trim($(this).html());
                  var op_val_arr = op_val.split("/");
                  
                  // console.log(op_val_arr);
                  var index_in_arr = $.trim(op_val_arr).indexOf(current_color);
                  if(index_in_arr > -1){
                      var option1 = $.trim(op_val_arr[0]);
                      var option2 = $.trim(op_val_arr[1]);
                    
                      $this.find("li.list-inline-item").each(function(){
                        if($(this).find('[data-single-option-selector]').val() == option1 || $(this).find('[data-single-option-selector]').val() == option2 ){
                        $(this).find('.size-swatch').addClass('not-available');
                        }
                      });
                  }
                }
            });
            // console.log(real_size_option_arr,"-----------------");
            // console.log(option1_size_all_arr,"+++++++++++");
            var differed_val_arr = $(real_size_option_arr).not(option1_size_all_arr).get();
            // differed_val_arr.each(function(i,e){
            //   console.log(this);
            // });
            // console.log(differed_val_arr.length);
            if(differed_val_arr.length > 0){
              $.each(differed_val_arr, function( index, value ) {
                var differed_val = value;
                $this.find("[data-for-size] li.list-inline-item").each(function(){
                  if($(this).find('.size-swatch input').val() == differed_val){
                    $(this).removeClass('d-block').addClass('d-none');
                  }
                });
              });
            }
          })
        }
        $('[data-bundles] [data-for-size] li input').on('click',function(){
          var val = $(this).val()
          var select = $(this).closest('[data-one-product]').find('.bundle-select-wrapper select option')
          $(this).closest('.selector-wrapper').find('[data-sizecode]').text(': '+val)
          $(select).each(function(){
            var selVal = $(this).val()
            if(val == selVal){
              $(this).closest('.single-option-selector')
              .val(selVal)
              .trigger('change')
            }
          })
        })

        $('[data-bundles] [data-for-color] li input').on('click',function(){
          var $changedSection = $(this);
          var val = $(this).val()
          var select = $(this).closest('[data-one-product]').find('.bundle-select-wrapper select option')
          $(this).closest('.selector-wrapper').find('[data-colorcode]').text(': '+val)
          $(select).each(function(){
            var selVal = $(this).val()
            if(val == selVal){
              $(this).closest('.single-option-selector')
              .val(selVal)
              .trigger('change')
            }
          })
// disable out of stock / unavailable variants for Bundle Products Begin 
          if($('#product-bundle').length > 0){
            // $('[data-one-product]').each(function(i,e){
              var $this = $changedSection.parents('[data-one-product]');
              $this.find('.size-swatch').removeClass('not-available');
              var current_color = $this.find(".color-swatch input[type='radio']:checked").val()
              // console.log('current color', current_color);
              var real_size_option_arr=[];
              var  option1_size_all_arr=[];
              
              $this.find("[data-for-size] li.list-inline-item").each(function(){
                $(this).removeClass('d-none');
                var real_size_option = $(this).find('.size-swatch input').removeClass('d-none').val();
                real_size_option_arr.push(real_size_option);
              });
              
            
              $this.find('[data-bundle-variant] option').each(function(i,e){
                var op_val_all = $.trim($(this).html());
                var op_val_arr_all = op_val_all.split("/");
                // console.log(op_val_arr_all, "============");
                var index_color_in_arr_all = $.trim(op_val_arr_all).indexOf(current_color);
                if(index_color_in_arr_all > -1){
                  var option1_all = $.trim(op_val_arr_all[0]);
                  var option2_all = $.trim(op_val_arr_all[1]);
                  // console.log(option1_all);
                  option1_size_all_arr.push(option1_all);
                }
                
                if ($(this).hasClass("unavailable")){
                  var op_val = $.trim($(this).html());
                  var op_val_arr = op_val.split("/");
                  // console.log(op_val_arr);
                  var index_in_arr = $.trim(op_val_arr).indexOf(current_color);
                  if(index_in_arr > -1){
                    var option1 = $.trim(op_val_arr[0]);
                    var option2 = $.trim(op_val_arr[1]);
                    $this.find("li.list-inline-item").each(function(){
                      // console.log($(this).find('[data-single-option-selector]').val());
                      if($(this).find('[data-single-option-selector]').val() == option1 || $(this).find('[data-single-option-selector]').val() == option2 ){
                          $(this).find('.size-swatch').addClass('not-available');
                      }
                    });
                  }
                }
              });

              // console.log(real_size_option_arr,"-----------------");
              // console.log(option1_size_all_arr,"+++++++++++");
              var differed_val_arr = $(real_size_option_arr).not(option1_size_all_arr).get();
              
              // console.log(differed_val_arr.length);
              if(differed_val_arr.length > 0){
                $.each(differed_val_arr, function( index, value ) {
                  var differed_val = value;
                  $this.find("[data-for-size] li.list-inline-item").each(function(){
                    if($(this).find('.size-swatch input').val() == differed_val){
                      $(this).removeClass('d-block').addClass('d-none');
                    }
                  });
                });
              }
              if($this.find("[data-for-size] li.list-inline-item.d-none:first input").is(":checked")){
                  $this.find("[data-for-size] li.list-inline-item:not(.d-none):first input").trigger('click');
              }
              // $this.find("[data-for-size] li.list-inline-item:not(.d-none):first input").trigger('click');
          }
// disable out of stock / unavailable variants for Bundle Products Begin 
        })
      },
  
      _variantChangeImage(){
        $('[data-for-color] input').on('click touchstart tap',function(){
          $(this).closest('[data-product-form]').find('[data-colorcode]').text(' : ' + $(this).val())
        })

        $('[data-for-size] input').on('click touchstart tap',function(){
          $(this).closest('[data-product-form]').find('[data-sizecode]').text(' : ' + $(this).val())
        })

        $("[data-for-simple] input").on('click touchstart tap',function(){
          var val = $(this).closest('.color-swatch').find('label').attr('data-original-title')

          if(val){
            let appendAll = '';
            var flkty1 = new Flickity('[data-carousel-nav]');

            flkty1.destroy()
            $('[data-thumbs]').each(function(index){
              var imgAlt = $(this).data('variant-title')

              imgAlt = imgAlt.replace(new RegExp('-', 'g'),' ')
              if (imgAlt === val) {
                appendAll = appendAll + $(this).html()
              }
            })
            // console.log(appendAll);
            $('[data-carousel-nav]').removeClass('flickity-enabled is-draggable').empty()
            $('[data-carousel-nav]').append(appendAll)
            $('[data-carousel-nav] [data-variant-title]:first').addClass('is-nav-selected')
            $('[data-carousel-main] img').attr('src',$('[data-carousel-nav] [data-variant-title]:first').data('src').replace('_650x',''))
            $('[data-carousel-main] img').attr('data-zoom',$('[data-carousel-nav] [data-variant-title]:first').data('src').replace('_650x',''))

            const flky = new Flickity('[data-carousel-nav]', {
                "contain": true, 
                "draggable": true,
                "cellAlign": "left",
                "pageDots": false,
                "adaptiveHeight": true,
                "percentPosition": false,
                "freeScroll": false,
                "groupCells": true,
                "wrapAround": false,
                "arrowShape": { 
                    "x0": 20,
                    "x1": 60, "y1": 40,
                    "x2": 65, "y2": 35,
                    "x3": 30
                  }
              })
          }
        })
      },

      _yotpo(){
        setTimeout(() => {
        var yotpoReviewText = $('.product-details-wrapper .yotpo .text-m').text().trim()
        if (yotpoReviewText != 'Write a review') {
          var yotpoReviewCount = $('.product-details-wrapper .yotpo .text-m').text().replace(' Reviews', '');
          $('.product-details-wrapper .yotpo .text-m').text('(' + yotpoReviewCount + ')');
          $('.product-details-wrapper .yotpo .text-m').show();
        }
      },1500);
      },

      _productNav(){
        $('body').on('click','[data-carousel-nav] [data-variant-title]',function(){
          $('[data-carousel-nav] [data-variant-title]').removeClass('is-nav-selected')
          $('[data-carousel-main] img').attr('src',$(this).data('src').replace('_650x',''))
          $('[data-carousel-main] img').attr('data-zoom',$(this).data('src').replace('_650x',''))
          $(this).closest('[data-variant-title]').addClass('is-nav-selected')
        })
      },

      _checkout(){
        $('[data-button-checkout]').click(function(){
          let discount = $('[data-discount]').val()
          window.location.href = '/checkout'+'?discount='+ discount
        });
        $('body').on('click','[data-scroll-color] [data-scroll-value]',function(){
          var pos=$(this).position().left; //get left position of li
          var currentscroll=$(this).parents('li').scrollLeft(); // get current scroll position
          var divwidth=$(this).parents('li').width(); //get div width
          pos=(pos+currentscroll)-(divwidth/2); // for center position if you want adjust then change this
  
          $(this).parents('li').animate({
            scrollLeft: pos
          });
  
        });
      },

      _triggers(){
        $('[data-main-nav]').meanmenu();
        $('[data-menu-toggler]').click(function(){
            $('[data-mobile-navigation]').addClass('opened');
            $('[data-site-overlay]').addClass('show');
            $('body').addClass('body-cart-open');
        });

        $('[data-cart-drawer]').click(function(e){
          e.stopPropagation();
        });

        $('html, [data-menu-close]').click(function(){
            $('[data-mobile-navigation]').removeClass('opened');
            $('[data-site-overlay]').removeClass('show');
            $('body').removeClass('menu-open body-cart-open');
        });

        $('[data-mobile-navigation], [data-menu-toggler]').click(function(event){
            event.stopPropagation();
        });

        $('[data-size-close]').click(function(){
          $('#exampleModalCenter').modal('hide');
        });
        
        $('[data-clear-all]').click(function(){
          window.location.href = '/collections/all';
        });
        
      },

      _faqs(){
        $('[data-target-button]').click(function(){
            $(this).find('i').toggleClass('fa-plus').toggleClass('fa-minus');
        });
      },

      _newsLetter(){
        var $form = $('[data-newsletter]')
        if ( $form.length > 0 ) {
         $form.find('[data-subscribe]').bind('click', function ( event ) {
          if ( event ) event.preventDefault()
          function validate_input(){
  
           var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i
           var get_email = $form.find('[data-email-input]').val()
           if(get_email != ''){
               if (testEmail.test(get_email)){
                register($form)
               }else{
                 $form.find('[data-error-msg]').text('Enter valid email.')
                 $form.find('[data-error-msg]').show('slow')
               }          
            }
            else{
                $form.find('[data-error-msg]').text('Enter email.')
                $form.find('[data-error-msg]').show('slow')
            }
          }
          validate_input()
         })
        }
        
        function register($form) {
          $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          data: $form.serialize(),
          cache       : false,
          dataType    : 'json',
          contentType: "application/json; charset=utf-8",
          success     : function(data) {
            if (data.result != "success") {
            $form.find('[data-error-msg]').html(data.msg).show()
            } else {
            $form.find('[data-error-msg]').html(data.msg).show().delay(2000).fadeOut()
            $form.find('[data-email-input]').val('')
            }
          }
          })
        }
      },

      _readMore(){
        if($('[data-read-more]').height() >= 260 ){
          $('[data-readmore]').closest('p').show()
          $('[data-read-more]').addClass('description-more')
        }else{
          $('[data-readmore]').closest('p').hide()
          $('[data-read-more]').removeClass('description-more')
        }

        $("body").on("click","[data-readmore]",function(){
          $('[data-read-more]').removeClass('description-more')
          $(this).removeAttr('data-readmore').attr('data-readless','').text('less')
        });

        $("body").on("click","[data-readless]",function(){
          $("[data-read-more]").addClass('description-more')
          $(this).attr('data-readmore','').removeAttr('data-readless').text('more')
        });
      },

      _colorslider(){
        setTimeout(function(){
          $("[data-color-choose]").niceScroll();
        }, 2000);
        $('[data-sizecode]').text(': '+$('[data-for-size] input:checked').val())
        $('[data-colorCode]').text(': '+$('[data-for-color] input:checked').val())
      },

      _colorSwatch(){
        $('body').on('click','[data-color-choose] > div',function(){
            var pos=$(this).position().left; //get left position of li
            var currentscroll=$(this).parents('[data-color-choose]').scrollLeft(); // get current scroll position
            var divwidth=$(this).parents('[data-color-choose]').width(); //get div width
            pos=(pos+currentscroll)-(divwidth/2); // for center position if you want adjust then change this
        
            $(this).parents('[data-color-choose]').animate({
              scrollLeft: pos
            });
          });
      },
      
      _forFilter(){
        $('[data-filter-btn]').click((e) => {
          $('[data-collection-filters]').addClass('side_toggle');
          $('body').addClass('body_overlay');
        });
        $('[data-back-arrow]').click((e) => {
          
          $('[data-collection-filters]').removeClass('side_toggle');
          $('body').removeClass('body_overlay'); 
        });
      },

      _arrowRotate(){
        $('body').on('click','[data-single-option-selector],[data-filter-sort]',function(){
          $(this).toggleClass('for-arrow');
        });
      },

      _heightForSlider(){
        var artHeight = $('[data-product-slider] .flickity-viewport').outerHeight();
        $('[data-product-slider] .flickity-viewport').css({
          'height' : artHeight
        });
        $('.product-slider .flickity-viewport').animate({ // quick and dirty
          'opacity': 1
        });
      },

      _sortingFilter(){
        var url = location.href;
        url = url.split('?sort_by=')[1];
        $('[data-sort-heapbox] [data-active-sort]').text(url)
        $('[data-sort-value] li').removeClass('active')
        $('[data-sort-value] li a').each(function(){
          if($(this).attr('data-value') == url){
            $(this).closest('li').addClass('active')
          }
        })
        $('[data-sort-heapbox] [data-sort-value] a').click(function(e){
          e.preventDefault()
          var val_sort = $(this).data('value') 
          $('[data-sort-heapbox] [data-active-sort]').text(val_sort)
          const sortValue = val_sort;
          if (sortValue) {
            Shopify.queryParams.sort_by = sortValue;
          } else {
            delete Shopify.queryParams.sort_by;
          }

          if (app.isCollectionAjaxed) {
            app._filterAjaxCall();
          } else {
            const search = $.param(Shopify.queryParams);
            if (search.length) {
              location.search = $.param(Shopify.queryParams);
            } else {
              location.href = location.pathname;
            }
          }
        })
      },

      _forDiscriptionBg(){
        setTimeout(() => {
          var height = $('.product-details-wrapper').outerHeight();
          $('[data-for-description-bg]').css('height',height);
        }, 1000);
      },

      _forCollectionsFilter(){
        if($(window).width() > 767){
          setTimeout(() => {
            var sidebar = new StickySidebar('[data-collection-sidebar]', {
              topSpacing: 20,
              bottomSpacing: 20,
              containerSelector: '.container',
              innerWrapperSelector: '[data-collection-filters]'
            });
          }, 1000);
        }
      },
      
    },
  }

export default customMethods;
