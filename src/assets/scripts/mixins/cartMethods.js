/*eslint import/no-cycle: 0*/
/**
 * Cart methods
 * -----------------------------------------------------------------------------
 *
 * @namespace cartMethods
 */

// date picker component
import Flickity from 'flickity';
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
import axios from 'axios';
import Vue from 'vue';

const cartMethods = {
  components: {
    flatPickr,
  },

  data() {
    return {
      // shipping calculator
      countries: window.Countries,
      provinces: null,
      selectedCountry: null,
      selectedProvince: null,
      selectedZip: null,
      zipRequired: false,
      shippingRates: null,
      shippingRatesErrors: null,

      // date picker
      // get more from https://chmln.github.io/flatpickr/options/
      datePickerConfig: {
        wrap: true, // set wrap to true only when using 'input-group'
        altFormat: 'M j, Y',
        altInput: true,
        dateFormat: 'm-d-Y',
        enableTime: false,
        locale: {
          firstDayOfWeek: 1, // start week on Monday
        },
        // minDate: "today",
        minDate: new Date().fp_incr(2), // 2 days from now

        // disable Saturdays and Sundays - disable using a function
        disable: [
          function(date) {
            // must return true to disable
            return (date.getDay() === 6 || date.getDay() === 0);
          },
        ],
      },
    };
  },

  watch: {
    selectedCountry() {
      const selectedCountryObj = app.countries[app.selectedCountry];
      const selectedProvincesObj = selectedCountryObj.province_alternate_names;
      const provincesLength = Object.keys(selectedProvincesObj).length;

      // clear data
      app.shippingRates = null;
      app.shippingRatesErrors = null;
      app.selectedZip = null;

      // set data
      app.zipRequired = selectedCountryObj.zip_required;

      if (provincesLength > 0) {
        app.provinces = selectedProvincesObj;
        // app.selectedProvince = Object.keys(app.provinces)[0]
      } else {
        app.provinces = null;
      }
    },
  },

  methods: {
    _initCart() {
      const isCartAjaxed = $('[data-template="cart"]').data('cart-ajaxed');

      if (isCartAjaxed) {
        app.isCartAjaxed = true;
      }
      if (app.currentTemplate == 'cart') {
        $(app.selectors.productObj).each((index, item) => {
          const productObj = $(item).html();
          const productData = JSON.parse(productObj);
          const product = productData.product;
          const optionsWithValues = productData.options_with_values;
          product.current_variant = product.variants[0];
          product.options_with_values = optionsWithValues;

          const selectedVarID = productData.product.id
          const jsonOfText = $('[data-'+selectedVarID+'-view-detail-json]')
          const jsonEleText = $(jsonOfText).text()
          if(jsonEleText){
            const jsonEleData = JSON.parse(jsonEleText);
            if(jsonEleData.reviews_average != ''){
              Vue.set(product, 'reviews_average', jsonEleData.reviews_average)
            }
            if(jsonEleData.reviews_count != ''){
              Vue.set(product, 'reviews_count', jsonEleData.reviews_count)
            }
          }

          // assign the product cart limit
          product.tags.forEach((tag) => {
            if (tag.match('cart-limit')) {
              const cartLimit = tag.replace('cart-limit-', '');
              product.cart_limit = parseFloat(cartLimit);
              //console.log(product);
            }
          });

          app.collectionProducts.push(product);
        });

        setTimeout(() => {
          const $carousel = $('.slider-flickity');
          if ($carousel.length > 0) {
            const flky = new Flickity($carousel[0], {
              adaptiveHeight: false,
              autoPlay: 5000,
              draggable: false,
              cellAlign: 'left',
              prevNextButtons: true,
              pageDots: false,
              contain: true,
              arrowShape: {
                x0: 10, x1: 60, y1: 50, x2: 60, y2: 45, x3: 15,
              },
            });
          }
        },500);
      }
      // GWP functionality
      app.discountCode = $('[data-template="cart"]').data('gwp-discount-code');
      app._getCartData();

      // shipping calculator
      app._initShippingCalculator();
    },

    /**
     * Get the cart data json object
     */
    _getCartData() {
      // IE11 bug: we need to disable cache for this request
      const config = {
        headers: {
          'Cache-Control': 'must-revalidate',
          'Cache-Control': 'no-cache',
          'Cache-Control': 'no-store',
          'Pragma': 'no-cache',
        },
      };

      axios.get('/cart.js', config)
        .then((response) => {
          app.cart = response.data;
          for(let i=0;i<response.data.items.length;i++){
          let handle = response.data.items[i].handle;
          let id = app.cart.items[i].id
          axios.get('/products/'+handle+'.js')
            .then((response) => {
              response.data.variants.forEach((variant, index) => {
                if(id == variant.id){
                  if(variant.compare_at_price != null){
                    Vue.set(app.cart.items[i], 'compare_at_price', variant.compare_at_price)
                  }else{
                    // $('[data-cart-item] strike').hide()
                  }
                  if(variant.inventory_quantity != null){
                    Vue.set(app.cart.items[i], 'inventory_quantity', variant.inventory_quantity)
                  }
                }
              })

              $('[data-varaint-show]').removeClass('d-none')

              response.data.options.forEach((option, index) => {
                if(response.data.options[index].name != 'Title'){
                  $(`[data-index="${handle}${index}"]`).text(response.data.options[index].name)
                }
              });
            });
          }
          return response;
        })
        .catch((error) => {
          // console.log(error)
          throw error;
        });
    },

    /**
     * Event to add a variant to the cart
     * adds item properties from hidden fields automatically
     *
     * @param  {number} variantId - current variant id
     */
    _addToCart(variantId, event) {
      const $form = $(event.currentTarget).closest(app.selectors.productForm);
      const $modal = $('[data-template="quickshop"]');
      const $qtyEl = $form.find(app.selectors.qtyInput);
      const qtyValue = $qtyEl.val() || 1;
      const productCartLimit = $(event.currentTarget).attr('data-cart-limit') || null;
      const cartProduct = app.cart.items.find(x => x.variant_id === variantId);
      const totalQty = (cartProduct ? Number(qtyValue) + Number(cartProduct.quantity) : qtyValue);
      const properties = {};
      const buttonTarget = $(event.currentTarget);
      Shopify.queue = [];

      $(buttonTarget).siblings('[data-adding]').text('Adding...').removeClass('adding-check')
      $(buttonTarget).addClass('adding-check')
      // add cart limit property
      properties['cart-limit'] = productCartLimit;

      // check if item has product limit assigned
      if (app._productHasCartLimit(totalQty, productCartLimit)) {
        return;
      }

      app.isLoading = true;
      app.isLoadingEvent = 'cartAdding';

      $.each($('input[name*="properties"]').serializeArray(), function() {
        const name = this.name.replace('properties[', '').replace(']', '');
        properties[name] = this.value;
      });

      // push the default variantId to the Shopify queue
      Shopify.queue.push({
        variantId,
      });

      // if there are selected product add-ons
      // push them also to the Shopify queue
      if (app.productAddons.length > 0) {
        app.productAddons.forEach((product) => {
          if (product.selected) {
            Shopify.queue.push({
              variantId: product.variants[0].id,
            });
          }
        });
      }

      Shopify.moveAlong = function() {
        // If we still have requests in the queue, let's process the next one.

        if (Shopify.queue.length) {
          const request = Shopify.queue.shift();

          axios.post('/cart/add.js', {
              quantity: qtyValue,
              id: request.variantId,
              properties,
            })
            .then((response) => {
              Shopify.moveAlong();
              return response;
            })
            .catch((error) => {
              // console.log(error)
              if (Shopify.queue.length) {
                Shopify.moveAlong();
              }
              throw error;
            });
        } else {
          app._getCartData();
          app._vibrateDevice();
          $qtyEl.val(1); // reset qty input to 1
          app.$snotify.clear();
          $('[data-template="quickshop"]').modal('hide');
          $(buttonTarget).siblings('[data-adding]').text('Added!')
          setTimeout(() => {
            app.isLoading = false;
            app.isLoadingEvent = null;
            app._toggleCartDrawer();
            $(buttonTarget).removeClass('adding-check')
            $(buttonTarget).siblings('[data-adding]').text('').addClass('adding-check')
          }, 500);

           setTimeout(() => {
            $('[data-add-to-cart]').fadeIn("slow");
          },3000);
        }
      };

      Shopify.moveAlong();
    },

    /**
     * Event to remove a variant from the cart
     *
     * @param  {number} itemId - line item index
     */
    _removeFromCart(itemIndex, event) {
      
      const $cartItem = $(event.currentTarget).parentsUntil(app.selectors.cartItem).parent();
      app.isLoading = true;
      $cartItem.addClass('updating');
      axios.post('/cart/change.js', {
          quantity: 0,
          line: itemIndex,
        })
        .then((response) => {
          app.isLoading = false;
          app._getCartData();
          $('[data-toggle="tooltip"]').tooltip('hide');

          return response;
        })
        .catch((error) => {
          // console.log(error)
          throw error;
        });
    },

    /**
     * Event to update a variant in the cart
     *
     * @param  {number} itemIndex - line item index
     */
    _updateCart(itemIndex, event) {
      const $qtyEl = $(event.currentTarget).parentsUntil(app.selectors.qtyContainer).find(app.selectors.qtyInput);
      const $cartItem = $(event.currentTarget).closest(app.selectors.cartItem);
      const productCartLimit = $cartItem.data('cart-limit') || null;
      let qtyValue = $qtyEl.val();

      if ($(event.currentTarget).data('qty-decrease') !== undefined) {
        if (parseFloat(qtyValue) >= 2) {
          qtyValue = parseFloat(qtyValue) - 1;
        }
      } else if ($(event.currentTarget).data('qty-increase') !== undefined) {
        
        qtyValue = parseFloat(qtyValue) + 1;
        // check if item has product limit assigned
        if (app._productHasCartLimit(qtyValue, productCartLimit)) {
          return;
        }
      }
      console.log(qtyValue)

      app.isLoading = true;
      $cartItem.addClass('updating');
      $($qtyEl).parent().css({
        'pointer-events': 'none',
      });

      console.log(qtyValue)
      axios.post('/cart/change.js', {
          quantity: qtyValue,
          line: itemIndex,
        })
        .then((response) => {
          app._getCartData();
          app.isLoading = false;
          $cartItem.removeClass('updating');
          $($qtyEl).parent().css({
            'pointer-events': 'inherit',
          });
          return response;
        })
        .catch((error) => {
          // console.log(error)
          throw error;
        });
    },

    _productHasCartLimit(qtyValue, productCartLimit) {
      //console.log(`qtyValue: ${qtyValue}, productCartLimit: ${productCartLimit}`);
      if (productCartLimit) {
        if (qtyValue > productCartLimit) {
          app.$snotify.warning(`Product cart limit applies for the specific product variant. You can't purchase more than ${productCartLimit} item(s) per order.`, 'Product Limit', {
            timeout: 10000,
            showProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            buttons: [{
              text: 'Close',
              bold: true,
              closeOnClick: true,
              // action: (toast) => {
              //   app.$snotify.remove(toast.id);
              // },
            }],
          });
          return true;
        }
      }
      return false;
    },
    /**
     * ShippingCalculator
     */
    _initShippingCalculator() {
      const $form = $('[data-shipping-calculator]');
      if ($form.length === 0) {
        return;
      }

      const customerCountry = $form.find('select[name="country"]').data('value').trim();
      const customerProvince = $form.find('select[name="province"]').data('value').trim();
      const customerZip = $form.find('input[name="zip"]').data('value').trim();

      // set date_ascending
      // if is user (is logged-in) show the user data
      // else get info from IP
      if (customerCountry) {
        app.selectedCountry = customerCountry;
        app.selectedProvince = customerProvince;
        app.selectedZip = customerZip;
      } else {
        axios.get('https://geoip-db.com/json/')
          .then((response) => {
            app.selectedCountry = response.data.country_name;
            app.selectedProvince = response.data.region_name;
            // app.selectedZip = response.data.zip_code
            return response;
          })
          .catch((error) => {
            // console.log(error)
            throw error;
          });
      }
    },

    _getCartShippingRates() {
      app.isLoading = true;

      axios.post('/cart/prepare_shipping_rates', {
          shipping_address: {
            country: app.selectedCountry,
            province: app.selectedProvince,
            zip: app.selectedZip,
          },
        })
        .then((response) => {
          app._pollCartShippingRates();
          return response;
        })
        .catch((error) => {
          // console.log(error.response.data)
          app.isLoading = false;
          app.shippingRatesErrors = error.response.data;
          throw error;
        });
    },

    _pollCartShippingRates() {
      axios.get('/cart/async_shipping_rates')
        .then((response) => {
          app.isLoading = false;
          if (response.data && response.data.shipping_rates) {
            app.shippingRates = response.data.shipping_rates;
          }
          return response;
        })
        .catch((error) => {
          // console.log(error)
          throw error;
        });
    },

    /**
     * Vibrate mobile device
     */
    _vibrateDevice() {
      // check for vibration support
      const vibrationEnabled = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
      if (vibrationEnabled) {
        navigator.vibrate(100);
      }
    },

  },
};

export default cartMethods;
