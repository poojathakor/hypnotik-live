// Older browsers support
import 'babel-polyfill';
import 'es6-promise/auto';

// Theme SCSS files
import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';
// Vue.js core
import Vue from 'vue';

// Bootstrap Framework
import 'bootstrap';
import '../vendor/jquery.nicescroll.min';
// import 'slick-carousel';

// Bootstrap-Vue
// import BootstrapVue from 'bootstrap-vue' // imports all library
// for better performance you can import individual components
import bPopover from 'bootstrap-vue/es/components/popover/popover';
import Vue2TouchEvents from 'vue2-touch-events';
import Snotify from 'vue-snotify';
import VueMatchHeights from 'vue-match-heights';

// Google Web font loader plugin
import WebFont from 'webfontloader';

// allow inline javascript code to be executed by using '<script2></script2>' tag
//import script2 from 'vue-script2';

// Lazysizes plugin for lazyloading
import 'lazysizes';
import 'lazysizes/plugins/unveilhooks/ls.unveilhooks'; // bg images support extension
import 'lazysizes/plugins/bgset/ls.bgset'; // responsive bg images support extension
import 'lazysizes/plugins/parent-fit/ls.parent-fit'; // parent fit extension

// Store => common data objects
import store from '../store/store';

// mixins
import helperMethods from '../mixins/helperMethods';
import layoutMethods from '../mixins/layoutMethods';
import cartMethods from '../mixins/cartMethods';
import collectionMethods from '../mixins/collectionMethods';
import productMethods from '../mixins/productMethods';
import accountMethods from '../mixins/accountMethods';
import quickShopMethods from '../mixins/quickShopMethods';
import wishlistMethods from '../mixins/wishlistMethods';
import recentlyViewedMethods from '../mixins/recentlyViewedMethods';
//import productReviewsMethods from '../mixins/productReviewsMethods';
import sectionMethods from '../mixins/sectionMethods';
import customMethods from '../mixins/customMethods';

let app;

// register components in Vue
//Vue.use(script2);
//Vue.use(BootstrapVue);
Vue.use(Vue2TouchEvents);
Vue.use(VueMatchHeights);
Vue.use(Snotify, {
  global: {
    preventDuplicates: true,
  },
  toast: {
    position: 'rightTop',
  },
});

// Create new Vue instance
new Vue({
  el: '#app',
  delimiters: ['${', '}'],

  components: {
    'b-popover': bPopover,
  },

  data() {
    return store; // shared data -> single source of truth
  },

  mixins: [
    helperMethods,
    layoutMethods,
    cartMethods,
    collectionMethods,
    productMethods,
    accountMethods,
    quickShopMethods,
    wishlistMethods,
    recentlyViewedMethods,
    //productReviewsMethods,
    sectionMethods,
    customMethods,
  ],

  mounted() {
    app = this;
    window.app = app;
    window.slate = window.slate || {};
    window.theme = window.theme || {};

    $(window).on('load', app._initTheme);
  },

  methods: {
    _initTheme() {
      $(app.selectors.sitePreloader).addClass('is-hidden'); // hide preloader

      // call helper function
      app._getCurrentTemplate();

      // initialization methods
      app._loadFonts(); // laod fonts asynchronously via Webfont loader
      app._initLayout(); // various functions related to layout, e.g. initialize Bootstrap specific components etc
      app._initCart(); // everything cart related, e.g. add product to cart etc
      app._initCollection(); // everything related to the collection pages, e.g. collection filtering etc
      app._initProduct(); // everything related to the product page, e.g. variant selection, product options, galleries etc
      app._initAccount(); // everything related to the user account page, login, register etc
      app._initWishlist(); // wishlist functionality
      app._initRecentlyViewed(); // recently viewed products functionality
      //app._initProductReviews(); // product reviews functionality
      app._initSection(); // Shopify admin sections & blocks => interaction between theme JavaScript and the theme editor
      app._initCustomMethods(); // custom JS code
    },

    // info how to load fonts => https://github.com/typekit/webfontloader
    _loadFonts() {
      WebFont.load({
         google: {
          families: [
             'Roboto Condensed:300,300i,400,400i,700,700i',
             'Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
           ],
         },
      });
    },
  },
});
