/**
 * Shared state data = single source of truth
 * -----------------------------------------------------------------------------
 *
 * @namespace store
 */

const store = {
  selectors: {
    sitePreloader: '[data-site-preloader]',
    promoProductsObj: '[data-cart-promos-json]',
    productObj: '[data-product-json]',
    relatedProductObj: '[data-related-product-json]',
    siteOverlay: '[data-site-overlay]',
    siteHeader: '[data-site-header]',
    headerSlider: '[data-header-slider]',
    searchLayer: '[data-search-layer]',
    newsletterPopup: '[data-newsletter-popup]',
    carousel: '[data-flickity]',
    productToolbarMobile: '[data-product-toolbar-mobile]',
    productTabs: '[data-product-tabs]',
    accountTabs: '[data-account-tabs]',
    singleOptionSelector: '[data-single-option-selector]',
    qtyContainer: '[data-qty-container]',
    qtyInput: '[data-qty-input]',
    cartItem: '[data-cart-item]',
    productForm: '[data-product-form]',
    cartForm: '[data-cart-form]',
    loginForm: '[data-login-form]',
    recoverForm: '[data-recover-form]',
    passwordInput: '[data-password-input]',
    instafeed: '[data-instafeed]',
    imageZoom: '[data-img-zoom]',
  },

  enableHistoryState: false,
  isLoading: false,
  loadingEvent: null,
  scrollPosition: 0,
  isLogoLight: false,
  isOverlayVisible: false,
  isCartDrawerOpen: false,
  isMobileNavDrawerOpen: false,
  isSearchLayerOpen: false,
  isNewsletterPopupOpen: false,
  isQuickshopModalOpen: false,
  isProductToolbarMobileOpen: false,
  isCollectionAjaxed: false,
  isCartAjaxed: false,
  currentTemplate: null,
  colorOptionIndex: 0,
  cart: window.theme.cart,
  moneyFormat: window.theme.moneyFormat,
  colorSwatchesEnabled: window.theme.colorSwatchesEnabled,
  sizeSwatchesEnabled: window.theme.sizeSwatchesEnabled,
  productExcludeTag: window.theme.productExcludeTag,
  shop: {},
  collectionProducts: [],
  relatedProducts: [],
  collectionPagination: {},
  activeFilters: [],
  product: {},
  currentVariant: {},
  productAddons: {},
  productAddonsTotalValue: 0,
  quickShop: {
    product: {},
    images: {},
    optionImages: {},
    currentVariant: {},
  },
  wishlist: [],
  recentlyViewed: []
};

module.exports = store;
