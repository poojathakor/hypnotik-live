import ShopifyAPI from 'shopify-node-api';

const clientShopify = new ShopifyAPI({
  shop: process.env.SLATE_STORE,
  shopify_api_key: process.env.SLATE_API_KEY,
  access_token: process.env.SLATE_PASSWORD,
  verbose: false, // logging output in console
});

// export
export default clientShopify;
