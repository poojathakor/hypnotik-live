import gql from 'graphql-tag';

export const getShop = gql`
  {
    shop {
      name
      description
      currencyCode
      moneyFormat
      primaryDomain {
        host
        sslEnabled
        url
      }
    }
  }
`;

export const customerByEmail = gql`
query ($first: Int!, $query: String!) {
  customers(first: $first, query: $query){
    edges {
      node {
        id
        firstName
        lastName
        displayName
        validEmailAddress
        email
        state
        tags
      }
    }
  }
}
`;
